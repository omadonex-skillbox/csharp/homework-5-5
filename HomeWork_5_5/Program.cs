﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_5_5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Task 1

            Console.WriteLine("Input a string:");
            string inputLine = Console.ReadLine();
            
            string[] words = SplitString(inputLine);

            Console.WriteLine("Splitted words:");
            PrintWords(words);

            //Task 2
            Console.WriteLine("Input a string:");
            inputLine = Console.ReadLine();
            Console.WriteLine("Reversed words:");
            Console.WriteLine(ReverseWords(inputLine));

            Console.ReadKey(true);
        }

        static string[] SplitString(string s)
        {
            return s.Split(' ');
        }

        static void PrintWords(string[] words)
        {
            foreach(string word in words)
            {
                Console.WriteLine(word);
            }
        }

        static string ReverseWords(string s)
        {
            string[] words = SplitString(s);
            
            return string.Join(" ", words.Reverse());
        }
    }
}
